import { FastifyInstance } from 'fastify'
import { randomUUID } from 'node:crypto'
import { z } from 'zod'

import { checkSessionIdExists } from '../middlewares/check-session-id-exists'
import { knex } from '../database'

export async function mealsRoutes(app: FastifyInstance) {
  app.post(
    '/',
    { preHandler: [checkSessionIdExists] },
    async (request, reply) => {
      const requestBodySchema = z.object({
        name: z.string(),
        description: z.string(),
        date: z.coerce.date(),
        onDiet: z.boolean(),
      })

      const { name, description, date, onDiet } = requestBodySchema.parse(
        request.body,
      )

      await knex('meals').insert({
        id: randomUUID(),
        user_id: request.user?.id,
        name,
        description,
        date: date.getTime(),
        on_diet: onDiet,
      })

      return reply.status(201).send()
    },
  )

  app.get('/', { preHandler: [checkSessionIdExists] }, async (request) => {
    const meals = await knex('meals')
      .where({ user_id: request.user?.id })
      .orderBy('date', 'desc')
      .select()

    return { meals }
  })

  app.get(
    '/:id',
    { preHandler: [checkSessionIdExists] },
    async (request, reply) => {
      const requestParamsSchema = z.object({
        id: z.string().uuid(),
      })

      const { id } = requestParamsSchema.parse(request.params)

      const meal = await knex('meals').where({ id }).first()

      if (!meal) return reply.status(404).send({ error: 'Meal not found.' })

      return reply.send({ meal })
    },
  )

  app.get(
    '/metrics',
    { preHandler: [checkSessionIdExists] },
    async (request) => {
      const meals = await knex('meals')
        .where({ user_id: request.user?.id })
        .orderBy('date', 'desc')
        .select()

      const { totalDietMeals, totalOffDietMeals, bestSequence } = meals.reduce(
        (acc, meal) => {
          if (meal.on_diet) {
            acc.totalDietMeals += 1
            acc.currentSequence += 1
          } else {
            acc.totalOffDietMeals += 1
            acc.currentSequence = 0
          }

          if (acc.currentSequence > acc.bestSequence)
            acc.bestSequence = acc.currentSequence

          return acc
        },
        {
          totalDietMeals: 0,
          totalOffDietMeals: 0,
          bestSequence: 0,
          currentSequence: 0,
        },
      )

      return {
        totalMeals: meals.length,
        totalDietMeals,
        totalOffDietMeals,
        bestSequence,
      }
    },
  )

  app.put(
    '/:id',
    { preHandler: [checkSessionIdExists] },
    async (request, reply) => {
      const requestParamsSchema = z.object({
        id: z.string().uuid(),
      })

      const { id } = requestParamsSchema.parse(request.params)

      const requestBodySchema = z.object({
        name: z.string(),
        description: z.string(),
        date: z.coerce.date(),
        onDiet: z.boolean(),
      })

      const { name, description, date, onDiet } = requestBodySchema.parse(
        request.body,
      )

      const meal = await knex('meals').where({ id }).first()

      if (!meal) return reply.status(404).send({ error: 'Meal not found.' })

      await knex('meals').where({ id }).update({
        name,
        description,
        date: date.getTime(),
        on_diet: onDiet,
      })

      return reply.status(204).send()
    },
  )

  app.delete(
    '/:id',
    { preHandler: [checkSessionIdExists] },
    async (request, reply) => {
      const requestParamsSchema = z.object({
        id: z.string().uuid(),
      })

      const { id } = requestParamsSchema.parse(request.params)

      const meal = await knex('meals').where({ id }).first()

      if (!meal) return reply.status(404).send({ error: 'Meal not found.' })

      await knex('meals').where({ id }).delete()

      return reply.status(204).send()
    },
  )
}
