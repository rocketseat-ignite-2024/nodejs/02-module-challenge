import { execSync } from 'node:child_process'
import request from 'supertest'

import { app } from '../src/app'
import { randomUUID } from 'node:crypto'

describe('Meals routes', () => {
  beforeAll(async () => await app.ready())

  afterAll(async () => app.close())

  beforeEach(() => {
    execSync('npm run knex migrate:rollback --all')
    execSync('npm run knex migrate:latest')
  })

  it('should be able to log a meal', async () => {
    const userResponse = await request(app.server)
      .post('/users')
      .send({
        name: 'John Doe',
        email: 'john.doe@example.com',
      })
      .expect(201)

    await request(app.server)
      .post('/meals')
      .set('Cookie', userResponse.get('Set-Cookie')!)
      .send({
        name: 'Meal #1',
        description: 'Description meal #1',
        date: new Date(),
        onDiet: true,
      })
      .expect(201)
  })

  it('should be list users meals', async () => {
    const userResponse = await request(app.server)
      .post('/users')
      .send({
        name: 'John Doe',
        email: 'john.doe@example.com',
      })
      .expect(201)

    await request(app.server)
      .post('/meals')
      .set('Cookie', userResponse.get('Set-Cookie')!)
      .send({
        name: 'Meal #1',
        description: 'Description meal #1',
        date: new Date(),
        onDiet: true,
      })
      .expect(201)

    await request(app.server)
      .post('/meals')
      .set('Cookie', userResponse.get('Set-Cookie')!)
      .send({
        name: 'Meal #2',
        description: 'Description meal #2',
        date: new Date(),
        onDiet: false,
      })
      .expect(201)

    const response = await request(app.server)
      .get('/meals')
      .set('Cookie', userResponse.get('Set-Cookie')!)
      .expect(200)

    expect(response.body.meals).toHaveLength(2)

    expect(response.body.meals[0].name).toBe('Meal #2')
    expect(response.body.meals[1].name).toBe('Meal #1')
  })

  it('should be able to show details of a specific meal', async () => {
    const userResponse = await request(app.server)
      .post('/users')
      .send({
        name: 'John Doe',
        email: 'john.doe@example.com',
      })
      .expect(201)

    await request(app.server)
      .post('/meals')
      .set('Cookie', userResponse.get('Set-Cookie')!)
      .send({
        name: 'Meal #1',
        description: 'Description meal #1',
        date: new Date(),
        onDiet: true,
      })
      .expect(201)

    const mealsResponse = await request(app.server)
      .get('/meals')
      .set('Cookie', userResponse.get('Set-Cookie')!)
      .expect(200)

    const { id: mealId } = mealsResponse.body.meals[0]

    const mealResponse = await request(app.server)
      .get(`/meals/${mealId}`)
      .set('Cookie', userResponse.get('Set-Cookie')!)
      .expect(200)

    expect(mealResponse.body).toEqual({
      meal: expect.objectContaining({
        name: 'Meal #1',
        description: 'Description meal #1',
        date: expect.any(Number),
        on_diet: 1,
      }),
    })
  })

  it('should not be able to show meal detail from another user', async () => {
    const userResponse = await request(app.server)
      .post('/users')
      .send({
        name: 'John Doe',
        email: 'john.doe@example.com',
      })
      .expect(201)

    await request(app.server)
      .post('/meals')
      .set('Cookie', userResponse.get('Set-Cookie')!)
      .send({
        name: 'Meal #1',
        description: 'Description meal #1',
        date: new Date(),
        onDiet: true,
      })
      .expect(201)

    const mealResponse = await request(app.server)
      .get(`/meals/${randomUUID()}`)
      .set('Cookie', userResponse.get('Set-Cookie')!)
      .expect(404)

    expect(mealResponse.body).toEqual({
      error: 'Meal not found.',
    })
  })

  it('should be able to update a specific meal', async () => {
    const userResponse = await request(app.server)
      .post('/users')
      .send({
        name: 'John Doe',
        email: 'john.doe@example.com',
      })
      .expect(201)

    await request(app.server)
      .post('/meals')
      .set('Cookie', userResponse.get('Set-Cookie')!)
      .send({
        name: 'Meal #1',
        description: 'Description meal #1',
        date: new Date(),
        onDiet: true,
      })
      .expect(201)

    const mealsResponse = await request(app.server)
      .get('/meals')
      .set('Cookie', userResponse.get('Set-Cookie')!)
      .expect(200)

    const { id: mealId } = mealsResponse.body.meals[0]

    await request(app.server)
      .put(`/meals/${mealId}`)
      .set('Cookie', userResponse.get('Set-Cookie')!)
      .send({
        name: 'Updated Meal #1',
        description: 'Updated description meal #1',
        date: new Date(),
        onDiet: false,
      })
      .expect(204)

    const mealResponse = await request(app.server)
      .get(`/meals/${mealId}`)
      .set('Cookie', userResponse.get('Set-Cookie')!)
      .expect(200)

    expect(mealResponse.body).toEqual({
      meal: expect.objectContaining({
        name: 'Updated Meal #1',
        description: 'Updated description meal #1',
        date: expect.any(Number),
        on_diet: 0,
      }),
    })
  })

  it('should be able to delete a specific meal', async () => {
    const userResponse = await request(app.server)
      .post('/users')
      .send({
        name: 'John Doe',
        email: 'john.doe@example.com',
      })
      .expect(201)

    await request(app.server)
      .post('/meals')
      .set('Cookie', userResponse.get('Set-Cookie')!)
      .send({
        name: 'Meal #1',
        description: 'Description meal #1',
        date: new Date(),
        onDiet: true,
      })
      .expect(201)

    const mealsResponse = await request(app.server)
      .get('/meals')
      .set('Cookie', userResponse.get('Set-Cookie')!)
      .expect(200)

    const { id: mealId } = mealsResponse.body.meals[0]

    await request(app.server)
      .delete(`/meals/${mealId}`)
      .set('Cookie', userResponse.get('Set-Cookie')!)
      .expect(204)
  })

  it('should be able to show the user metrics', async () => {
    const userResponse = await request(app.server)
      .post('/users')
      .send({
        name: 'John Doe',
        email: 'john.doe@example.com',
      })
      .expect(201)

    await request(app.server)
      .post('/meals')
      .set('Cookie', userResponse.get('Set-Cookie')!)
      .send({
        name: 'Meal #1',
        description: 'Description meal #1',
        date: new Date('2024-05-30T12:30:00'),
        onDiet: true,
      })
      .expect(201)

    await request(app.server)
      .post('/meals')
      .set('Cookie', userResponse.get('Set-Cookie')!)
      .send({
        name: 'Meal #2',
        description: 'Description meal #2',
        date: new Date('2024-05-30T20:00:00'),
        onDiet: true,
      })
      .expect(201)

    await request(app.server)
      .post('/meals')
      .set('Cookie', userResponse.get('Set-Cookie')!)
      .send({
        name: 'Meal #3',
        description: 'Description meal #3',
        date: new Date('2024-05-31T08:45:00'),
        onDiet: false,
      })
      .expect(201)

    await request(app.server)
      .post('/meals')
      .set('Cookie', userResponse.get('Set-Cookie')!)
      .send({
        name: 'Meal #4',
        description: 'Description meal #4',
        date: new Date('2024-05-31T13:15:00'),
        onDiet: true,
      })
      .expect(201)

    const response = await request(app.server)
      .get('/meals/metrics')
      .set('Cookie', userResponse.get('Set-Cookie')!)
      .expect(200)

    expect(response.body).toEqual({
      totalMeals: 4,
      totalDietMeals: 3,
      totalOffDietMeals: 1,
      bestSequence: 2,
    })
  })
})
